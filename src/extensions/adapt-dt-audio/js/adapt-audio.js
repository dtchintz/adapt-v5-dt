define(function(require) {
  var Adapt = require('coreJS/adapt');
  var Backbone = require('backbone');
  var AudioView = Backbone.View.extend({
    self: this,
    className: "audio-play",
    initialize: function() {
      this.render();
      this.audioPlay();

    },
    render: function() {

    },
    audioPlay: function() {
      var self = this;
      $(document).on('click', '.audio-play', function(event) {
        var audiotag = document.getElementsByTagName("audio");
        for (var i = 0; i < audiotag.length; i++) {
          audiotag[i].pause();
        }
        var audio = $(event.target).parent();
        var $currentAudio = audio.find('audio');
        var $slider = audio.find('input');
        audio.find('audio').trigger('play');
        $currentAudio[0].addEventListener("timeupdate", function() {
          var nt = $currentAudio[0].currentTime * (100 / $currentAudio[0].duration);
          $slider[0].value = nt;
        });
      });
      $(document).on('click', '.audio-pause', function(event) {
        var audio = $(event.target).parent();
        audio.find('audio').trigger('pause');
      });
      $(document).on('click', '.audio-replay', function(event) {
        var audio = $(event.target).parent();
        var $currentAudio = audio.find('audio');
        var $slider = audio.find('input');
        audio.find('audio').trigger('pause');
        var resetAudio = setInterval(function() {

          var newVal = $slider.val(function(i, val) {
            if ($slider[0].value <= 0) {
              $currentAudio[0].currentTime = 0;
              clearInterval(resetAudio);
              return;
            }
            return +val - 1;
          });
        }, 1);
      });
    },
    udpateSlider: function(slider, audio) {}
  });
  Adapt.once("app:dataLoaded", function() {
    new AudioView();
  });
});
